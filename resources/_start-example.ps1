Clear-Host

$executable = "RustDedicated.exe"
$installDir = Split-Path $PSCommandPath -Parent
$logFile = "$installDir\RustDedicated_Data\output_log.txt"
$args = @"
-batchmode -nographics
+rcon.ip 0.0.0.0
+rcon.port 28016
+rcon.password "CHANGEME"
+server.ip 0.0.0.0
+server.port 28015
+server.maxplayers 10
+server.hostname "My uMod Server"
+server.identity "my_server_identity"
+server.level "Procedural Map"
+server.seed 12345
+server.worldsize 4000
+server.saveinterval 300
+server.globalchat true
+server.description "Powered by uMod"
+server.headerimage "https://assets.umod.org/images/umod-rust-header.png"
+server.url "https://umod.org"
"@

do {
    $process = Get-Process | Where-Object { $_.Path -Like "*$executable*" } -ErrorAction SilentlyContinue
    if ($process) {
        Write-Host "Stopping existing server(s)...`n"
        $process | Stop-Process -Force; Start-Sleep 5
    }

    Write-Host "Starting server...`n"
    $serverJob = Start-Job -ScriptBlock { Start-Process "$using:installDir\$using:executable" -ArgumentList $using:args -Wait }

    $logJob = Start-Job -ScriptBlock { Get-Content "$using:logFile" -Wait }
    while ($serverJob.State -eq 'Running' -And $logJob.HasMoreData) {
      Receive-Job $logJob
      Start-Sleep -Milliseconds 200
    }

    Receive-Job $logJob; Stop-Job $logJob; Remove-Job $logJob; Remove-Job $serverJob -Force
    Write-Host "Restarting server...`n"; Start-Sleep 5
} while (!($serverJob.State -eq 'Running'))
