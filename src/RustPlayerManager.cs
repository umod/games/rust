﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Rust
{
    /// <summary>
    /// Represents a Rust player manager
    /// </summary>
    public class RustPlayerManager : PlayerManager<RustPlayer>
    {
        /// <summary>
        /// Create a new instance of the RustPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public RustPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, RustPlayer rustPlayer, int playerFilter = 0)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, rustPlayer, playerFilter))
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Id) != 0 && rustPlayer.basePlayer?.net?.connection != null)
            {
                return rustPlayer.basePlayer.net.ID.ToString().Equals(partialNameOrIdOrIp);
            }

            return false;
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <param name="playerFilter"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player, int playerFilter = 0)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player, playerFilter))
            {
                return true;
            }

            if ((playerFilter & (int)PlayerFilter.Id) != 0 && player is RustPlayer rustPlayer && rustPlayer.basePlayer?.net?.connection != null)
            {
                return rustPlayer.basePlayer.net.ID.ToString().Equals(partialNameOrIdOrIp);
            }

            return false;
        }
    }
}
