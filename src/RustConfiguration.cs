﻿using uMod.Configuration.Toml;
using uMod.IO;

namespace uMod.Game.Rust
{
    [TomlComment("In-game chat options")]
    public class RustChatConfiguration
    {
        [TomlProperty("avatar")]
        public ulong Avatar { get; internal set; } = 76561198127163614;
    }

    public class RustConfiguration : TomlFile
    {
        /// <summary>
        /// Whether or not server is modded
        /// </summary>
        [TomlProperty("modded")]
        public bool Modded { get; }

        /// <summary>
        /// Default chat configuration
        /// </summary>
        [TomlProperty("chat")]
        public RustChatConfiguration Chat { get; }

        /// <summary>
        /// Create a new instance of a server configuration
        /// </summary>
        /// <param name="filename"></param>
        public RustConfiguration(string filename) : base(filename)
        {
            Modded = true;
            Chat = new RustChatConfiguration
            {
                Avatar = 76561198127163614
            };
        }
    }
}
