﻿using System.IO;
using uMod.Common;
using uMod.Common.Command;
using uMod.IO;
using uMod.Text;
using uMod.Utilities;

namespace uMod.Game.Rust
{
    /// <summary>
    /// Provides Universal functionality for the game server
    /// </summary>
    public class RustProvider : IUniversalProvider
    {
        /// <summary>
        /// Gets the name of the game for which this provider provides
        /// </summary>
        public string GameName => "Rust";

        /// <summary>
        /// Gets the Steam app ID of the game's client, if available
        /// </summary>
        public uint ClientAppId => 252490;

        /// <summary>
        /// Gets the Steam app ID of the game's server, if available
        /// </summary>
        public uint ServerAppId => 258550;

        /// <summary>
        /// Gets a container with important game-specific types
        /// </summary>
        public IGameTypes Types { get; private set; }

        /// <summary>
        /// The current rust configuration
        /// </summary>
        internal RustConfiguration Config;

        /// <summary>
        /// Gets the singleton instance of this provider
        /// </summary>
        internal static RustProvider Instance { get; private set; }

        public RustProvider()
        {
            Instance = this;
        }

        /// <summary>
        /// Gets the command system provider
        /// </summary>
        public RustCommands CommandSystem { get; private set; }

        /// <summary>
        /// Creates the game-specific command system provider object
        /// </summary>
        /// <param name="commandHandler"></param>
        /// <returns></returns>
        public ICommandSystem CreateCommandSystemProvider(ICommandHandler commandHandler) => CommandSystem = new RustCommands(commandHandler);

        /// <summary>
        /// Creates the game specific player manager
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public IPlayerManager CreatePlayerManager(IApplication application, ILogger logger)
        {
            return new RustPlayerManager(application, logger);
        }

        /// <summary>
        /// Creates the game-specific server object
        /// </summary>
        /// <returns></returns>
        public IServer CreateServer()
        {
            IServer server = new RustServer();
            Types = new GameTypes(server)
            {
                Player = typeof(BasePlayer)
            };

            Config = DataFile.Load<RustConfiguration>(Path.Combine(Interface.uMod.InstanceDirectory, "rust.toml"));
            return server;
        }

        /// <summary>
        /// Formats the text with universal markup as game-specific markup
        /// </summary>
        /// <param name="text">text to format</param>
        /// <returns>formatted text</returns>
        public string FormatText(string text) => Formatter.ToUnity(text);
    }
}
